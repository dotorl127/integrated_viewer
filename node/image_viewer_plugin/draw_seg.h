#ifndef DRAW_SEG_H
#define DRAW_SEG_H

#include <opencv/cv.h>

namespace integrated_viewer {
  // helper class to draw points image
  class DrawSeg{
  public:
    explicit DrawSeg(void);
    void Draw(cv::Mat& seg, cv::Mat& image, float opacity);

  private:
    cv::Mat color_map_;

  };

} // end namespace integrated_viewer

#endif // DRAW_POINTS_H
