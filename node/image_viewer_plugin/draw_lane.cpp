#include "draw_lane.h"

#include <opencv/cv.hpp>
#include <opencv2/core/version.hpp>

#if (CV_MAJOR_VERSION != 3)
#include <opencv2/contrib/contrib.hpp>
#endif

namespace integrated_viewer
{
  const int DrawLane::kLineThickness = 3;
  const cv::Scalar DrawLane::kRed = CV_RGB(255, 0, 0);

  DrawLane::DrawLane(void) {
  }  // DrawLane::DrawLane()

  void DrawLane::Draw(const keti_msgs::ProjectedLanes::ConstPtr& lanes, cv::Mat &image) {
    if (lanes == NULL)
    {
      return;
    }
    else
    {
      for(int i = 0; i < lanes->lanes.size(); i++)
      {
        for(int j = 1; j < lanes->lanes[i].lane.size(); j++)
        {
          cv::Point point_start(lanes->lanes[i].lane[j - 1].x, lanes->lanes[i].lane[j - 1].y);
          cv::Point point_end(lanes->lanes[i].lane[j].x, lanes->lanes[i].lane[j].y);
          cv::line(image, point_start, point_end, kRed, kLineThickness);
        }
      }
    }

    // TODO : change draw lane code

  }  // void DrawLane::Draw()
}
