#ifndef IMAGE_VIEWER_PLUGIN_H
#define IMAGE_VIEWER_PLUGIN_H

#ifndef Q_MOC_RUN
#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <rviz/panel.h>

#include <string>
#include <map>

#include <opencv/cv.h>
#include <opencv/highgui.h>

#include <QStringList>
#include <QWidget>
#include <QEvent>
#include <keti_msgs/PerceivedObjectArray.h>
#include <keti_msgs/ProjectedPoints.h>
#include <keti_msgs/ProjectedLanes.h>

#include "convert_image.h"
#include "ui_image_viewer_form.h"
#include "draw_rects.h"
#include "draw_points.h"
#include "draw_lane.h"
#include "draw_seg.h"


#endif

namespace integrated_viewer
{

class ImageViewerPlugin: public rviz::Panel {
  Q_OBJECT
public:
  explicit ImageViewerPlugin(QWidget* parent = 0);

  // override resize event
  virtual void resizeEvent(QResizeEvent *);

protected:
  // The function to update topic list that can be selected from the UI
  void UpdateTopicList(void);

  // The event filter to catch clicking on combo box
  bool eventFilter(QObject* object, QEvent* event);

  // The Callback functions
  void ImageCallback(const sensor_msgs::CompressedImage::ConstPtr& msg);
  void DetectedObjCallback(const keti_msgs::PerceivedObjectArray::ConstPtr &msg);
  void DetectedMarkerCallback(const keti_msgs::PerceivedObjectArray::ConstPtr &msg);
  void PointCallback(const keti_msgs::ProjectedPoints::ConstPtr &msg);
  void LaneCallback(const keti_msgs::ProjectedLanes::ConstPtr& msg);
  void SegCallback(const sensor_msgs::Image::ConstPtr& msg);

  // The function to refrect modified image on UI
  void ShowImageOnUi(void);

  // The data type of the topic that will be shown in each combo box
  static const QString kImageDataType;
  static const QString kDetectedObjectDataTypeBase;
  static const QString kPointDataType;
  static const QString kLaneDataType;
  static const QString kSegDataType;

  // The blank topic name
  static const QString kBlankTopic;

  // The ROS node handle.
  ros::NodeHandle node_handle_;

  // The ROS subscriber
  ros::Subscriber image_sub_;
  ros::Subscriber rect_sub_;
  ros::Subscriber marker_sub_;
  ros::Subscriber point_sub_;
  ros::Subscriber lane_sub_;
  ros::Subscriber seg_sub_;

  // Save and load overrides
  virtual void save(rviz::Config config) const;
  virtual void load(const rviz::Config& config);

private:
  // The UI components
  Ui::image_viewer_form ui_;

  // The image displayed on viewer
  cv::Mat viewed_image_;
  cv::Mat default_image_;

  ros::Publisher image_topic_pub_;
  std::string image_topic_name;

  // Data pointer to hold subscribed data
  keti_msgs::ProjectedPoints::ConstPtr points_msg_;
  keti_msgs::PerceivedObjectArray::ConstPtr detected_objects_msg_, detected_marker_msg_;
  keti_msgs::ProjectedLanes::ConstPtr lane_msg_;
  cv::Mat seg_msg_;

  // The helper-class constructor for drawing
  DrawRects rects_drawer_;
  DrawRects marker_rects_drawer_;
  DrawPoints points_drawer_;
  DrawLane lane_drawer_;
  DrawSeg seg_drawer_;

  // The flag to represent whether default image should be shown or not
  bool default_image_shown_;

  // The behavior definition of the UI
private Q_SLOTS:
  // We can skip "connect" process by defining naming
  // of slot function like on_"widget_name"_"signal_name"
  void on_image_topic_combo_box__activated(int index);
  void on_rect_topic_combo_box__activated(int index);
  void on_marker_topic_combo_box__activated(int index);
  void on_point_topic_combo_box__activated(int index);
  void on_lane_topic_combo_box__activated(int index);
  void on_seg_topic_combo_box__activated(int index);
}; // end class ImageViewerPlugin

} // end namespace integrated_viewer

#endif // IMAGE_VIEWER_PLUGIN_H
