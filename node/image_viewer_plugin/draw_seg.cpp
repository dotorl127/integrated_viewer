#include "draw_seg.h"
#include <opencv2/opencv.hpp>

namespace integrated_viewer
{
  DrawSeg::DrawSeg(void) {}

  void DrawSeg::Draw(cv::Mat &seg, cv::Mat &image, float opacity)
  {
    if(seg.empty())
    {
      return;
    }
    else
    {
      if(image.size().width != seg.size().width)
        cv::resize(seg, seg, cv::Size(image.size().width, seg.size().height), 0, 0, CV_INTER_NN);
      if(image.size().height != seg.size().height)
        cv::resize(seg, seg, cv::Size(seg.size().width, image.size().height), 0, 0, CV_INTER_NN);

      cv::addWeighted(image, (1 - opacity), seg, opacity, 0, image);
    }
  } // DrawPoints::Draw()

} // end namespace integrated_viewer
